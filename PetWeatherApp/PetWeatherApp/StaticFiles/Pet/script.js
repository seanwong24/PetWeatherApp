﻿var petTypeToString = function (petType) {
    switch (petType) {
        case 0:
            return "Dog";
        case 1:
            return "Cat";
    }
};
var petTypeToInt = function (petType) {
    switch (petType) {
        case "Dog":
            return 0;
        case "Cat":
            return 1;
        default:
            return -1;
    }
};

var petApp = angular.module("petApp", []);

var listCtril = petApp.controller("listCtrl", function ($scope, $http) {
    $http.get('https://petshelter-api.herokuapp.com/pet')
        .then(function (response) {
            var list = response.data;
            for (var i = 0; i < list.length; i++) {
                list[i].type = petTypeToString(list[i].type);
            }
            $scope.list = list;
        });
    $scope.viewDetail = function (id) {
        window.location.href = "pet/" + id;
    }
});

var addCtrl = petApp.controller("addCtrl", function ($scope, $http) {
    $scope.resetHelpTexts = function () {
        $scope.nameHelpText = "";
        $scope.typeHelpText = "";
        $scope.breedHelpText = "";
        $scope.latitudeHelpText = "";
        $scope.longitudeHelpText = "";
    };
    $scope.checkValues = function (pet) {
        var result = true;
        if (pet.name == null || pet.name == "") {
            $scope.nameHelpText = "Please enter a name.";
            result = false;
        }
        if (pet.type == -1) {
            $scope.typeHelpText = "Please select a type.";
            result = false;
        }
        if (pet.breed == "Choose...") {
            $scope.breedHelpText = "Please select a breed.";
            result = false;
        }
        if (pet.latitude == null || pet.latitude == "") {
            $scope.latitudeHelpText = "Please enter a latitude.";
            result = false;
        }
        if (pet.longitude == null || pet.longitude == "") {
            $scope.longitudeHelpText = "Please enter a longitude.";
            result = false;
        }

        return result;
    };
    $scope.typeList = ["Choose...", "Dog", "Cat"];
    $scope.type = "Choose...";
    $scope.breedList = ["Choose..."];
    $scope.breed = "Choose...";
    $scope.getBreedList = function () {
        switch ($scope.type) {
            case "Dog":
                return ["Choose...", "Beagle", "Brittany", "Pointer", "Shiba"];
            case "Cat":
                return ["Choose...", "Balinese", "Sphynx"];
            default:
                return ["Choose..."];
        }
    }
    $scope.confirm = function () {
        $scope.resetHelpTexts();
        var pet = new Object();
        pet.name = $scope.name;
        pet.type = petTypeToInt($scope.type);
        pet.breed = $scope.breed;
        pet.latitude = $scope.latitude;
        pet.longitude = $scope.longitude;

        if ($scope.checkValues(pet)) {
            var data = JSON.stringify(pet);
            $http.post('https://petshelter-api.herokuapp.com/pet/add', data)
                .then(function (response) {
                    if (response.data == "true") {
                        alert("Add successful");
                    }
                    else {
                        alert("Fail to add new pet, please check " + response.data + ".");
                    }
                })
        }
    };
    $http.get('https://petshelter-api.herokuapp.com/pet')
        .then(function (response) {
            var list = response.data;
            for (var i = 0; i < list.length; i++) {
                list[i].type = petTypeToString(list[i].type);
            }
            $scope.list = list;
        });
});

var detailCtrl = petApp.controller("detailCtrl", function ($scope, $http) {
    $scope.id = window.location.pathname.split("pet/")[1];
    $http.get('https://petshelter-api.herokuapp.com/pet/' + $scope.id)
        .then(function (response) {
            var pet = response.data;
            pet.type = petTypeToString(pet.type);
            $scope.pet = pet;
            $scope.obtainForecast();
        });
    $scope.obtainForecast = function () {
        var url = "/forecast/israining"
            + "/" + $scope.pet.latitude + "/" + $scope.pet.longitude;
        $http.get(url).then(function (response) {
            $scope.header = JSON.parse(response.data) ? "Yup!" : "Nope!";
            $scope.infoText = "It looks like "
                + $scope.pet.name + " "
                + (JSON.parse(response.data) ? "is going to" : "does not")
                + " need one in "
                + $scope.pet.location + ".";
        })
    };
});