﻿namespace Utilities {
    export abstract class Controller {
        protected abstract route: string;
        protected app;
        protected express;
        init(app, express) {
            this.app = app;
            this.express = express;
            this.setActions();
        };
        abstract setActions();
        get(route: string, handler: (req, res) => void): void {
            this.app.get(this.route + route, handler);
        };
        put(route: string, handler: (req, res) => void): void {
            this.app.put(this.route + route, handler);
        };
        post(route: string, handler: (req, res) => void): void {
            this.app.post(this.route + route, handler);
        };
        delete(route: string, handler: (req, res) => void): void {
            this.app.delete(this.route + route, handler);
        };
        staticContent(route: string, path: string) {
            this.app.use(this.route + route, this.express.static(path));
        }
    }
}
