/// <reference path="./Services/ServerHost.ts" />
/// <reference path="./Controller/PetController.ts" />
/// <reference path="./Controller/ForecastController.ts" />

var port = process.env.PORT || 10086;

new Services.ServerHost()
    .addController(new Controllers.PetController())
    .addController(new Controllers.ForecastController())
    .start(port);
