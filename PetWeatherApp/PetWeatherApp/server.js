var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var Services;
(function (Services) {
    class ServerHost {
        constructor() {
            this.express = require("express");
            this.app = this.express();
        }
        addController(controller) {
            controller.init(this.app, this.express);
            console.log("Controller added.");
            return this;
        }
        start(port) {
            try {
                this.app.listen(port);
            }
            catch (e) {
                console.log(e);
            }
            console.log("Server started, now listening on port " + port + ".");
        }
    }
    Services.ServerHost = ServerHost;
})(Services || (Services = {}));
var Utilities;
(function (Utilities) {
    class Controller {
        init(app, express) {
            this.app = app;
            this.express = express;
            this.setActions();
        }
        ;
        get(route, handler) {
            this.app.get(this.route + route, handler);
        }
        ;
        put(route, handler) {
            this.app.put(this.route + route, handler);
        }
        ;
        post(route, handler) {
            this.app.post(this.route + route, handler);
        }
        ;
        delete(route, handler) {
            this.app.delete(this.route + route, handler);
        }
        ;
        staticContent(route, path) {
            this.app.use(this.route + route, this.express.static(path));
        }
    }
    Utilities.Controller = Controller;
})(Utilities || (Utilities = {}));
/// <reference path="../Utilities/Controller.ts" />
var Controllers;
(function (Controllers) {
    class PetController extends Utilities.Controller {
        constructor() {
            super(...arguments);
            this.route = "/pet";
        }
        setActions() {
            // add views
            this.staticContent("", "./StaticFiles/Pet/list.html");
            this.staticContent("/new", "./StaticFiles/Pet/add.html");
            this.staticContent("/:id", "./StaticFiles/Pet/detail.html");
            // add static files
            this.staticContent("/files", "./StaticFiles/Pet");
        }
    }
    Controllers.PetController = PetController;
})(Controllers || (Controllers = {}));
var Services;
(function (Services) {
    class Forecast {
        static obtainForecast(latitude, longitude) {
            return __awaiter(this, void 0, void 0, function* () {
                var url = "https://api.darksky.net/forecast/5cff5ee8cfdf80455d2238fe553161c9"
                    + "/" + latitude + "," + longitude;
                var data = yield Forecast.request(url);
                return JSON.parse(data);
            });
        }
        static isRaining(latitude, longitude) {
            return __awaiter(this, void 0, void 0, function* () {
                var obj = yield Forecast.obtainForecast(latitude, longitude);
                return obj.currently.icon == "rain";
            });
        }
    }
    Forecast.request = require("request-promise");
    Services.Forecast = Forecast;
})(Services || (Services = {}));
/// <reference path="../Utilities/Controller.ts" />
/// <reference path="../Services/Forecast.ts" />
var Controllers;
(function (Controllers) {
    class ForecastController extends Utilities.Controller {
        constructor() {
            super(...arguments);
            this.route = "/forecast";
        }
        setActions() {
            this.get("/:latitude/:longitude", function (req, res) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        var obj = yield Services.Forecast.obtainForecast(req.params.latitude, req.params.longitude);
                    }
                    catch (e) {
                        res.end(e.toString());
                    }
                    res.end(JSON.stringify(obj));
                });
            });
            this.get("/isRaining/:latitude/:longitude", function (req, res) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        var obj = yield Services.Forecast.isRaining(req.params.latitude, req.params.longitude);
                    }
                    catch (e) {
                        res.end("unknown");
                    }
                    res.end(JSON.stringify(obj));
                });
            });
        }
    }
    Controllers.ForecastController = ForecastController;
})(Controllers || (Controllers = {}));
/// <reference path="./Services/ServerHost.ts" />
/// <reference path="./Controller/PetController.ts" />
/// <reference path="./Controller/ForecastController.ts" />
var port = process.env.PORT || 10086;
new Services.ServerHost()
    .addController(new Controllers.PetController())
    .addController(new Controllers.ForecastController())
    .start(port);
/// <reference path="../Utilities/Controller.ts" />
var Controllers;
(function (Controllers) {
    class DefaultController extends Utilities.Controller {
        constructor() {
            super(...arguments);
            this.route = "/";
        }
        setActions() {
            // add view
            this.staticContent("", "./StaticFiles/Default/index.html");
        }
    }
    Controllers.DefaultController = DefaultController;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=server.js.map