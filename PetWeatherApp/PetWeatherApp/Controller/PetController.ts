﻿/// <reference path="../Utilities/Controller.ts" />

namespace Controllers {
    export class PetController extends Utilities.Controller {
        protected route: string = "/pet";
        setActions() {
            // add views
            this.staticContent("", "./StaticFiles/Pet/list.html");
            this.staticContent("/new", "./StaticFiles/Pet/add.html");
            this.staticContent("/:id", "./StaticFiles/Pet/detail.html");

            // add static files
            this.staticContent("/files", "./StaticFiles/Pet");
        }
    }
}
