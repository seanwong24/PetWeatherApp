﻿/// <reference path="../Utilities/Controller.ts" />

namespace Controllers {
    export class DefaultController extends Utilities.Controller {
        protected route: string = "/";
        setActions() {
            // add view
            this.staticContent("", "./StaticFiles/Default/index.html");
        }
    }
}
