﻿/// <reference path="../Utilities/Controller.ts" />
/// <reference path="../Services/Forecast.ts" />

namespace Controllers {
    export class ForecastController extends Utilities.Controller {
        protected route: string = "/forecast";
        setActions() {
            this.get("/:latitude/:longitude", async function (req, res) {
                try {
                    var obj = await Services.Forecast.obtainForecast(req.params.latitude, req.params.longitude);
                } catch (e) {
                    res.end(e.toString());
                }
                res.end(JSON.stringify(obj));
            })
            this.get("/isRaining/:latitude/:longitude", async function (req, res) {
                try {
                    var obj = await Services.Forecast.isRaining(req.params.latitude, req.params.longitude);
                } catch (e) {
                    res.end("unknown");
                }
                res.end(JSON.stringify(obj));
            })
        }
    }
}