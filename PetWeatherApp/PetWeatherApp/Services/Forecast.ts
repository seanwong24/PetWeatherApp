﻿﻿namespace Services {
    export class Forecast {
        static request = require("request-promise");
        static async obtainForecast(latitude: number, longitude: number): Promise<any> {
            var url = "https://api.darksky.net/forecast/5cff5ee8cfdf80455d2238fe553161c9"
                + "/" + latitude + "," + longitude;
            var data = await Forecast.request(url);
            return JSON.parse(data);
        }
        static async isRaining(latitude: number, longitude: number): Promise<boolean> {
            var obj = await Forecast.obtainForecast(latitude, longitude);
            return obj.currently.icon == "rain";
        }
    }
}