﻿namespace Services {
    export class ServerHost {
        express;
        app;

        constructor() {
            this.express = require("express");
            this.app = this.express();
        }

        addController(controller: Utilities.Controller): ServerHost {
            controller.init(this.app, this.express);
            console.log("Controller added.");
            return this;
        }

        start(port) {
            try {
                this.app.listen(port);
            } catch (e) {
                console.log(e);
            }
            console.log("Server started, now listening on port " + port + ".");
        }
    }
}
