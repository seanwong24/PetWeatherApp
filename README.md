# PetWeatherApp
The is a web app using PetShelterAPI which provides 3 pages:
  1. An index of Pets 
  2. Pet lookup by ID and check if it needs a unbrella 
  3. Pet creation
  
To start it, simply navigate to the source folder where a "server.js" file exists and run "node server.js"
